#! /usr/bin/python
# -*- coding: utf-8 -*-

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
import unittest, time, re

class SeleniumXataka(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "http://duckduckgo.com/"
        self.verificationErrors = []
    
    def test_selenium_xataka(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_id("search_form_input_homepage").clear()
        driver.find_element_by_id("search_form_input_homepage").send_keys("xataka")
        driver.find_element_by_id("search_button_homepage").click()
        # ERROR: Caught exception [ERROR: Unsupported command [isTextPresent]]
        # Lo mejor = driver.find_element_by_link_text("Lo mejor").text
        driver.find_element_by_link_text("Gadgets y tecnología: últimas tecnologías en electrónica ...").click()
        Lomejor = driver.find_element_by_link_text("Lo mejor").text
        Facebook = self.is_element_present(By.CSS_SELECTOR, "a[title=\"Conéctate a la página de Xataka en Facebook\"] > span")
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
